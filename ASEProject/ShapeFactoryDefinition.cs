﻿using System;///you are using the System library in your project
using System.Collections.Generic;///allow users to create strongly typed collections that provide better type safety and performance than non-generic strongly typed collections
using System.Linq;/// Provides classes and interfaces that support queries that use Language-Integrated Query (LINQ)
using System.Text;/// Contains classes that represent ASCII and Unicode character encodings
using System.Threading.Tasks; ///Provides a pool of threads that can be used to execute tasks, post work items, process asynchronous I/O

namespace AseProject ///namespaces are used to logically arrange classes, structs, interfaces, enums and delegates
{
    ///simple class Factory Definition class
    public class ShapeFactoryDefinition
    {
        /// <summary>
        /// method to check if shape is circle
        /// <param name="shape"></param>
        /// </summary>
        /// <returns></returns>    
      
        public bool isCircle(string shape) ///it will check weather the shape is is circle or not.If shape is circle return True esle False
        {
            if(shape == "circle")
            {
                return true;
            }
            return false;
        }
        ///<summary>
        /// Method to check if shape is rectangle 
        /// <param name="shape"></param>
        /// </summary>
        /// <returns></returns>

        public bool isRectangle(string shape) ///it will check weather the shape is Rectangle or not.If shape is rectangle return True esle False
        {
            if (shape == "rectangle")
            {
                return true;
            }
            return false;
        }

        
        public bool isTriangle(string Shape) ///it will check weather the shape is is triangle or not.If shape is triangle return True esle False
        {
            if (Shape =="Triangle")
            {
                return true;
            }
            return false;
        }

    }
}
