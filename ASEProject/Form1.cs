﻿using System;///you are using the System library in your project
using System.Collections.Generic;///allow users to create strongly typed collections that provide better type safety and performance than non-generic strongly typed collections
using System.ComponentModel;///Provides classes that are used to implement the run-time and design-time behavior of components and controls
using System.Data;///Provides access to classes that represent the ADO.NET architecture. 
using System.Drawing;///The Graphics class provides methods for drawing to the display device. Classes such as Rectangle and Point encapsulate GDI+ primitives
using System.IO;///The System.IO namespace consists of IO related classes, structures, delegates and enumerations. These classes can be used to reads and write data to files or data streams.
using System.Linq;/// Provides classes and interfaces that support queries that use Language-Integrated Query (LINQ)
using System.Text;/// Contains classes that represent ASCII and Unicode character encodings
using System.Threading.Tasks;  ///Provides a pool of threads that can be used to execute tasks, post work items, process asynchronous I/O
using System.Text.RegularExpressions; ///The Regex class represents the .NET Framework's regular expression engine. It can be used to quickly parse large amounts of text to find specific character
using System.Windows.Forms;///Contains classes for creating Windows-based applications that take full advantage of the rich user interface features available in the Microsoft Windows operating system.
/// </summary>


namespace AseProject ///namespaces are used to logically arrange classes, structs, interfaces, enums and delegates
{
    public partial class FrmGPLA : Form
    {
        //declaration of variable for shapes
        Circle circle;
        Rectangle rectangle;
        Polygon polygon;
        //end of variable declaration

        Boolean drawCircle, drawRectangle, drawPolygon, drawLine;//boolean values for checking shapes
        String program;//string to hold textarea info
        String[] words;//words of individual program
        int moveX, moveY;//cursor moving direction points
        int thickness;//thickness of pen


        String line = "";
        int loopCounter = 0;
        String active = "pen";
        Boolean hasDrawOrMoveValue = false;

        public int radius = 0;
        public int length = 0;
        public int breadth = 0;
        public int dSize = 0;
        public int num = 0;
        public int counter = 0;

        Graphics g;
        Color backcolor = Color.Black;
        string shape;
        ShapeFactory shapeFactory = new ShapeFactory();
        Shape shapes;

        Color btnBorderColor = Color.FromArgb(0, 0, 0);
        Color color;
        Point point;//point on drawaing panel
        string actionCmd;
        string consoletext;

        Shape shape1, shape2;//shapefactory declaration
        //Individual object list for shapes
        List<Circle> circleObjects;
        List<Rectangle> rectangleObjects;
        List<Polygon> polygonObjects;
        List<MoveDirection> moveObjects;

      

        public FrmGPLA()
        {
            InitializeComponent();
            g = pnlDisplayOutput.CreateGraphics();

            //Shape Creation and initialization
            AbstractFactory shapeFactory = FactoryProducer.getFactory("Shape");
            shape1 = shapeFactory.getShape("Circle");
            shape2 = shapeFactory.getShape("Rectangle");
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Take a guide:\n" +
               "to draw circle 100\n" +
               "to draw rectangle 100 50\n" +
               "to draw polygon\n" +
               "to moveto 100 90\n" +
               "color green 2\n");
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog.FileName, txtCodeArea.Text);
            }
        }

       
       
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //opens dialogbox when button is cliced to load the code
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtCodeArea.Text = File.ReadAllText(openFileDialog1.FileName);
            }
        }

      

        private void FrmGPLA_Load(object sender, EventArgs e)
        {
            circle = new Circle(); //instantiating circle
            circleObjects = new List<Circle>();// declaration of array of circle objects
            rectangleObjects = new List<Rectangle>();// declaration of array of rectabgle objects
            polygonObjects = new List<Polygon>();//declaration of array for polygon objects          
            moveObjects = new List<MoveDirection>();//declaration of array for moving objects
            color = Color.White;
        }
        public class Command
        {
            public static IDictionary<String, int> storeVariables = new Dictionary<String, int>();

            /// <summary>
            /// Object of <see cref="ShapeFactory"/>. <br/>
            /// Access the methods and objects of <see cref="ShapeFactory"/> class from <see cref="CommandParser"/> class.
            /// </summary>
            ShapeFactory identifierObject = new ShapeFactory();

            /// <summary>
            /// Stores boolean values: true when method statement is encountered in the program window.
            /// </summary>
            bool methodFlag = false;

            /// <summary>
            /// Object of <see cref="MethodParser"/> class to access method within the class.
            /// </summary>
            MethodParser methods = new MethodParser();

            /// <summary>
            /// Stores string value which stores the name of method entered by the user.
            /// </summary>
            String methodName;

            /// <summary>
            /// Method: Triggered when enteredCode has var and expression separated by space and are required to be split.
            /// Splits the var and expression separated by space as different item and saves them into an array of string.
            /// </summary>
            /// <param name="varExpression"> Holds the string value which contains var and expression separated by space</param>
            /// <returns>The string array which contains var and expression separate values as different items</returns>
            public String[] splitVariableExpression(String varExpression)
            {
                // Array of strings which stores code, separated by space, as a different item. 
                String[] splittedVarExp = varExpression.Split(' ');

                return splittedVarExp;
            }

            /// <summary>
            /// Method: Triggered when expression has values separated by '=' and are required to be split.
            /// Splits the parameters separated by '=' as a different item and saves them into an array of string.
            /// </summary>
            /// <param name="expression"> Holds the string value of expression which are separated by '=' </param>
            /// <returns> The string array which contains expression's separated values as different items.</returns>
            public String[] splitExpression(String expression)
            {
                // Array of strings which stores code, separated by '=', as a different item. 
                String[] splittedExpression = expression.Split('=');

                return splittedExpression;
            }

            /// <summary>
            /// Method: Triggered when enteredCode has command and parameters separated by space and are required to be split.
            /// Splits the command and parameters separated by space as different item and saves them into an array of string.
            /// </summary>
            /// <param name="enteredCode"> Holds the string value which contains command and parameters separated by space</param>
            /// <returns>The string array which contains commands and parameters separate values as different items</returns>
            public String[] CommandSplitter(String enteredCode)
            {
                /// Array of strings which stores code, separated by space, as a different item. 
                String[] splittedCommand = enteredCode.Split(' ');

                return splittedCommand;
            }

            /// <summary>
            /// Method: Triggered when parameters has values separated by ',' and are required to be split.
            /// Splits the parameters separated by ',' as a different item and saves them into an array of string.
            /// </summary>
            /// <param name="parameters">Holds the string value of parameters which are separated by ',' </param>
            /// <returns> The string array which contains parameter's separated values as different items.</returns>
            public String[] ParameterSplitter(String parameters)
            {
                // Array of strings which stores parameters, separated by ',' as a different item.
                String[] splittedParameter = parameters.Split(',');

                return splittedParameter;
            }

            /// <summary>
            /// Method Triggered when some text is written in the program Windown and run command is pressed. <br/>
            /// Checks the entered Code line by line for built in commands like 'var', 'if statement', 'while statement' and also performs variable operations like (+), (-), (*) and (/)
            /// </summary>
            /// <param name="enteredCode">Holds each line of command retrieved from the program Window of the application</param>
            /// <param name="lineCounter">Holds the number of line of the text</param>
            /// <param name="syntaxButton">Holds the boolean value of syntaxButton which confirms if syntaxButton was pressed in the application</param>
            public void commandReader(String enteredCode, int lineCounter, bool syntaxButton)
            {

                // Splits the enteredCode with '(' and stores in tempName
                String tempName = enteredCode.Split('(')[0];

                // Stores the first word of the enteredCode of every line in the Program Window to check for built in commands like if, method or while.
                String declareName = enteredCode.Split(' ')[0];

                // Checks if the keywoord is method and performs task underneath.
                if (declareName.Equals("method"))
                {
                    // calls a method of performMethod class which checks and splits method statements and returns true when method is found and false otherwise.
                    methodFlag = methods.identifyMethod(enteredCode, syntaxButton, lineCounter);
                    // Retrieves the name of method and stores in the string variable.
                    methodName = methods.methodName;
                }
                // Checks if the methodFlag
                else if (methodFlag)
                {
                    // calls a method of performMethod class which checks and returns true if the endmethod statement is encountered and false otherwise.
                    methodFlag = methods.storeMethodCommands(enteredCode);
                }
                // Checks if the entered code is methodname as method is called if so and performs the task underneath.
                else if (enteredCode.Equals(methodName + "()"))
                {
                    //Iterates to retrieve each line after method statement and before endmethod
                    foreach (String eachLineCode in methods.methodCommands)
                    {
                        // Calls commandReader to execute each line retrieved from the arraylIst of codes.
                        commandReader(eachLineCode, lineCounter, syntaxButton);
                    }
                }
                // Checks if the entered code is methodName with parameters and and performs task underneath
                else if (tempName.Equals(methodName))
                {
                    try
                    {
                        // splits the enteredcode with '(' and stores in string array.
                        String[] splittedCallMethod = enteredCode.Split('(');

                        // Splits the enteredcode with respect to '(' and ')' and stores second value in methodParameters
                        String methodParameters = enteredCode.Split('(', ')')[1];
                        // Splits all parameters with comma and stores in string array.
                        String[] parameters = methodParameters.Split(',');

                        // Checks if the mehtod name matches with entered methodname.
                        if (splittedCallMethod[0].Equals(methodName))
                        {
                            // Checks if the parameters lenght are equal
                            if (parameters.Length == methods.splittedParameters.Length)
                            {
                                // Iterates to take each parameter and performs task underneath
                                for (int index = 0; index < parameters.Length; index++)
                                {
                                    // Matches and assigns values of parameters to variable declared while declaring method
                                    storeVariables[methods.splittedParameters[index]] = Convert.ToInt32(parameters[index]);

                                    // Checks if the stored variable dictionary contains the entered variable as key.
                                    if (storeVariables.ContainsKey(methods.splittedParameters[index]))
                                    {
                                        // checks the value of the variable stored in dictionary.
                                        methods.splittedParameters[index] = storeVariables[methods.splittedParameters[index]].ToString();
                                    }
                                }

                                // Iterates to retrieve each line of code under method and above endmethod and also runs.
                                foreach (String eachLineCode in methods.methodCommands)
                                {
                                    // Calls the commandReader method to execute all command inside the arraylist.
                                    commandReader(eachLineCode, lineCounter, syntaxButton);
                                }
                            }
                            else
                            {
                                // Throws user defiended exception.
                                MessageBox.Show("Invalid Method Command");
                            }
                        }
                    }
                    // catchees
                    catch (IndexOutOfRangeException)
                    {
                        // Adds the following line as error in the errorList
                        //errorList.Add("ERROR!!! AT LINE " + lineCounter + ". Please Enter Correct Method statement");
                    }

                }

                // Checks if the specific line of enteredCode contains '+' sign and performs tasks underneath.
                else if (enteredCode.Contains("+"))
                {
                    // Splits the statement with respect to the '+' sign and stores separated values in the string array
                    String[] expParameter = enteredCode.Split('+');

                    // Checks if the first parameter in the string array is a variable stored in data dictionary and performs the tasks underneath.
                    if (storeVariables.ContainsKey(expParameter[0]))
                    {
                        // Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
                        if (storeVariables.ContainsKey(expParameter[1]))
                        {
                            // Adds the values entered along with the operator and hence changes the value stored in that particular variable.
                            storeVariables[expParameter[0]] = storeVariables[expParameter[0]] + storeVariables[expParameter[1]];
                        }
                        else
                        {
                            // Adds the values entered along with the operator and hence changes the value stored in that particular variable.
                            storeVariables[expParameter[0]] = storeVariables[expParameter[0]] + Convert.ToInt32(expParameter[1]);
                        }
                    }
                    else
                    {
                        // Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
                        if (storeVariables.ContainsKey(expParameter[1]))
                        {
                            // Adds the values entered along with the operator and hence changes the value stored in that particular variable.
                            storeVariables[expParameter[1]] = storeVariables[expParameter[1]] + Convert.ToInt32(expParameter[0]);
                        }
                    }
                }
                // Checks if the specific line of enteredCode contains '-' sign and performs tasks underneath.
                else if (enteredCode.Contains("-"))
                {
                    // Splits the statement with respect to the '-' sign and stores separated values in the string array
                    String[] expParameter = enteredCode.Split('-');

                    //Checks if the first parameter in the string array is a variable stored in data dictionary and performs the tasks underneath.
                    if (storeVariables.ContainsKey(expParameter[0]))
                    {
                        // Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
                        if (storeVariables.ContainsKey(expParameter[1]))
                        {
                            // Adds the values entered along with the operator and hence changes the value stored in that particular variable.
                            storeVariables[expParameter[0]] = storeVariables[expParameter[0]] - storeVariables[expParameter[1]];
                        }
                        else
                        {
                            // Adds the values entered along with the operator and hence changes the value stored in that particular variable.
                            storeVariables[expParameter[0]] = storeVariables[expParameter[0]] - Convert.ToInt32(expParameter[1]);
                        }
                    }
                    else
                    {
                        // Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
                        if (storeVariables.ContainsKey(expParameter[1]))
                        {
                            // Adds the values entered along with the operator and hence changes the value stored in that particular variable.
                            storeVariables[expParameter[1]] = storeVariables[expParameter[1]] - Convert.ToInt32(expParameter[0]);
                        }
                    }
                }
                // Checks if the specific line of enteredCode contains '*' sign and performs tasks underneath.
                else if (enteredCode.Contains("*"))
                {
                    // Splits the statement with respect to the '*' sign and stores separated values in the string array
                    String[] expParameter = enteredCode.Split('*');

                    // Checks if the first parameter in the string array is a variable stored in data dictionary and performs the tasks underneath.
                    if (storeVariables.ContainsKey(expParameter[0]))
                    {
                        // Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
                        if (storeVariables.ContainsKey(expParameter[1]))
                        {
                            // Adds the values entered along with the operator and hence changes the value stored in that particular variable.
                            storeVariables[expParameter[0]] = storeVariables[expParameter[0]] * storeVariables[expParameter[1]];
                        }
                        else
                        {
                            // Adds the values entered along with the operator and hence changes the value stored in that particular variable.
                            storeVariables[expParameter[0]] = storeVariables[expParameter[0]] * Convert.ToInt32(expParameter[1]);
                        }
                    }
                    else
                    {
                        // Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
                        if (storeVariables.ContainsKey(expParameter[1]))
                        {
                            // Adds the values entered along with the operator and hence changes the value stored in that particular variable.
                            storeVariables[expParameter[1]] = storeVariables[expParameter[1]] * Convert.ToInt32(expParameter[0]);
                        }
                    }
                }
                // Checks if the specific line of enteredCode contains '/' sign and performs tasks underneath.
                else if (enteredCode.Contains("/"))
                {
                    // Splits the statement with respect to the '/' sign and stores separated values in the string array
                    String[] expParameter = enteredCode.Split('/');

                    // Checks if the first parameter in the string array is a variable stored in data dictionary and performs the tasks underneath.
                    if (storeVariables.ContainsKey(expParameter[0]))
                    {
                        // Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
                        if (storeVariables.ContainsKey(expParameter[1]))
                        {
                            // Adds the values entered along with the operator and hence changes the value stored in that particular variable.
                            storeVariables[expParameter[0]] = storeVariables[expParameter[0]] / storeVariables[expParameter[1]];
                        }
                        else
                        {
                            // Adds the values entered along with the operator and hence changes the value stored in that particular variable.
                            storeVariables[expParameter[0]] = storeVariables[expParameter[0]] / Convert.ToInt32(expParameter[1]);
                        }
                    }
                    else
                    {
                        // Checks if the second parameter in the string array is also a variable and performs the tasks underneath.
                        if (storeVariables.ContainsKey(expParameter[1]))
                        {
                            // Adds the values entered along with the operator and hence changes the value stored in that particular variable.
                            storeVariables[expParameter[1]] = storeVariables[expParameter[1]] / Convert.ToInt32(expParameter[0]);
                        }
                    }
                }

            }
        }

        /// <summary>
        /// load the commands to draw the shape
        /// </summary>
        private void loadCommand()
        {
            int numberOfLines = txtCodeArea.Lines.Length;

            for (int i = 0; i < numberOfLines; i++)
            {
                String oneLineCommand = txtCodeArea.Lines[i];
                oneLineCommand = oneLineCommand.Trim();
                if (!oneLineCommand.Equals(""))
                {
                    Boolean hasDrawto = Regex.IsMatch(oneLineCommand.ToLower(), @"\bdrawto\b");
                    Boolean hasMoveto = Regex.IsMatch(oneLineCommand.ToLower(), @"\bmoveto\b");
                    if (hasDrawto || hasMoveto)
                    {
                        String args = oneLineCommand.Substring(6, (oneLineCommand.Length - 6));
                        String[] parms = args.Split(',');
                        for (int j = 0; j < parms.Length; j++)
                        {
                            parms[j] = parms[j].Trim();
                        }
                        moveX = int.Parse(parms[0]);
                        moveY = int.Parse(parms[1]);
                        hasDrawOrMoveValue = true;
                    }
                    else
                    {
                        hasDrawOrMoveValue = false;
                    }
                    if (hasMoveto)
                    {
                        pnlDisplayOutput.Refresh();
                    }
                }
            }

            for (loopCounter = 0; loopCounter < numberOfLines; loopCounter++)
            {
                String oneLineCommand = txtCodeArea.Lines[loopCounter];
                oneLineCommand = oneLineCommand.Trim();
                if (!oneLineCommand.Equals(""))
                {
                    RunCommand(oneLineCommand);
                }

            }
        }

        /// <summary>
        /// run command using to execute when click on execute button
        /// </summary>
        /// <param name="oneLineCommand"></param>
        private void RunCommand(String oneLineCommand)
        {

            Boolean hasPlus = oneLineCommand.Contains('+');
            Boolean hasEquals = oneLineCommand.Contains("=");
            if (hasEquals)
            {
                oneLineCommand = Regex.Replace(oneLineCommand, @"\s+", " ");
                string[] words = oneLineCommand.Split(' ');
                //removing white spaces in between words
                for (int i = 0; i < words.Length; i++)
                {
                    words[i] = words[i].Trim();
                }
                String firstWord = words[0].ToLower();
                if (firstWord.Equals("if"))
                {
                    Boolean loop = false;
                    if (words[1].ToLower().Equals("radius"))
                    {
                        if (radius == int.Parse(words[3]))
                        {
                            loop = true;
                        }
                    }
                    else if (words[1].ToLower().Equals("breadth"))
                    {
                        if (breadth == int.Parse(words[3]))
                        {
                            loop = true;
                        }
                    }
                    else if (words[1].ToLower().Equals("length"))
                    {
                        if (length == int.Parse(words[3]))
                        {
                            loop = true;
                        }

                    }
                    else if (words[1].ToLower().Equals("counter"))
                    {
                        if (counter == int.Parse(words[3]))
                        {
                            loop = true;
                        }
                    }
                    else if (words[1].ToLower().Equals("num"))
                    {
                        if (counter == int.Parse(words[3]))
                        {
                            loop = true;
                        }
                    }

                    int ifStartLine = (GetIfStartLineNumber());
                    int ifEndLine = (GetEndifEndLineNumber() - 1);
                    loopCounter = ifEndLine;
                    if (loop)
                    {
                        for (int j = ifStartLine; j <= ifEndLine; j++)
                        {
                            string oneLineCommand1 = txtCodeArea.Lines[j];
                            oneLineCommand1 = oneLineCommand1.Trim();
                            if (!oneLineCommand1.Equals(""))
                            {
                                RunCommand(oneLineCommand1);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("If Statement is false"); // checks if statement is true otherwise throw this message
                    }
                }
                else
                {
                    string[] words2 = oneLineCommand.Split('=');
                    for (int j = 0; j < words2.Length; j++)
                    {
                        words2[j] = words2[j].Trim();
                    }
                    if (words2[0].ToLower().Equals("radius"))
                    {
                        radius = int.Parse(words2[1]);
                    }
                    else if (words2[0].ToLower().Equals("breadth"))
                    {
                        breadth = int.Parse(words2[1]);
                    }
                    else if (words2[0].ToLower().Equals("length"))
                    {
                        length = int.Parse(words2[1]);
                    }
                    else if (words2[0].ToLower().Equals("counter"))
                    {
                        counter = int.Parse(words2[1]);
                    }
                    else if (words2[0].ToLower().Equals("num"))
                    {
                        num = int.Parse(words2[1]);
                    }


                }
            }
            else if (hasPlus)
            {
                oneLineCommand = System.Text.RegularExpressions.Regex.Replace(oneLineCommand, @"\s+", " ");
                string[] words = oneLineCommand.Split(' ');
                if (words[0].ToLower().Equals("repeat"))
                {
                    counter = int.Parse(words[1]);
                    if (words[2].ToLower().Equals("circle"))
                    {
                        int increaseValue = GetSize(oneLineCommand);
                        radius = increaseValue;
                        for (int j = 0; j < counter; j++)
                        {
                            DrawCircle(radius);
                            radius += increaseValue;
                        }
                    }
                    else if (words[2].ToLower().Equals("rectangle"))
                    {
                        int increaseValue = GetSize(oneLineCommand);
                        dSize = increaseValue;
                        for (int j = 0; j < counter; j++)
                        {
                            DrawRectangle(dSize, dSize);
                            dSize += increaseValue;
                        }
                    }
                    else if (words[2].ToLower().Equals("triangle"))
                    {
                        int increaseValue = GetSize(oneLineCommand);
                        dSize = increaseValue;
                        for (int j = 0; j < counter; j++)
                        {
                            DrawTriangle(dSize, dSize, dSize);
                            dSize += increaseValue;
                        }
                    }
                }
                else
                {
                    string[] words2 = oneLineCommand.Split('+');
                    for (int j = 0; j < words2.Length; j++)
                    {
                        words2[j] = words2[j].Trim();
                    }
                    if (words2[0].ToLower().Equals("radius"))
                    {
                        radius += int.Parse(words2[1]);
                    }
                    else if (words2[0].ToLower().Equals("breadth"))
                    {
                        breadth += int.Parse(words2[1]);
                    }
                    else if (words2[0].ToLower().Equals("length"))
                    {
                        length += int.Parse(words2[1]);
                    }
                    
                }
            }
            else
            {
                sendDrawCommand(oneLineCommand);
            }
        }

        /// <summary>
        /// Returns the size of the shapes
        /// </summary>
        /// <param name="lineCommand"></param>
        /// <returns></returns>
        private int GetSize(string lineCommand)
        {
            int value = 0;
            if (lineCommand.ToLower().Contains("radius"))
            {
                int pos = (lineCommand.IndexOf("radius") + 6);
                int size = lineCommand.Length;
                String tempLine = lineCommand.Substring(pos, (size - pos));
                tempLine = tempLine.Trim();
                String newTempLine = tempLine.Substring(1, (tempLine.Length - 1));
                newTempLine = newTempLine.Trim();
                value = int.Parse(newTempLine);

            }
            else if (lineCommand.ToLower().Contains("size"))
            {
                int pos = (lineCommand.IndexOf("size") + 4);
                int size = lineCommand.Length;
                String tempLine = lineCommand.Substring(pos, (size - pos));
                tempLine = tempLine.Trim();
                String newTempLine = tempLine.Substring(1, (tempLine.Length - 1));
                newTempLine = newTempLine.Trim();
                value = int.Parse(newTempLine);
            }
            return value;
        }

        /// <summary>
        /// Method to lets us draw the different shapes
        /// </summary>
        /// <param name="lineOfCommand"></param>
        private void sendDrawCommand(string lineOfCommand)
        {
            String[] shapes = { "circle", "rectangle", "triangle" };
            String[] variable = { "radius", "breadth", "length","counter", "size","num" };

            lineOfCommand = System.Text.RegularExpressions.Regex.Replace(lineOfCommand, @"\s+", " ");
            string[] words = lineOfCommand.Split(' ');
            //removing white spaces in between words
            for (int i = 0; i < words.Length; i++)
            {
                words[i] = words[i].Trim();
            }
            String firstWord = words[0].ToLower();
            Boolean firstWordShape = shapes.Contains(firstWord);
            if (firstWordShape)
            {

                if (firstWord.Equals("circle"))
                {
                    Boolean secondWordIsVariable = variable.Contains(words[1].ToLower());
                    if (secondWordIsVariable)
                    {
                        if (words[1].ToLower().Equals("radius"))
                        {
                            DrawCircle(radius);
                        }
                        else if (words[1].ToLower().Equals("num"))
                        {
                            DrawCircle(num);
                        }
                       
                    }
                    else
                    {
                        DrawCircle(Int32.Parse(words[1]));
                    }

                }
                else if (firstWord.Equals("rectangle"))
                {
                    String args = lineOfCommand.Substring(9, (lineOfCommand.Length - 9));
                    String[] parms = args.Split(',');
                    for (int i = 0; i < parms.Length; i++)
                    {
                        parms[i] = parms[i].Trim();
                    }
                    Boolean secondWordIsVariable = variable.Contains(parms[0].ToLower());
                    Boolean thirdWordIsVariable = variable.Contains(parms[1].ToLower());
                    if (secondWordIsVariable)
                    {
                        if (thirdWordIsVariable)
                        {
                            DrawRectangle(breadth, length);
                        }
                        else
                        {
                            DrawRectangle(breadth, Int32.Parse(parms[1]));
                        }

                    }
                    else
                    {
                        if (thirdWordIsVariable)
                        {
                            DrawRectangle(Int32.Parse(parms[0]), length);
                        }
                        else
                        {
                            DrawRectangle(Int32.Parse(parms[0]), Int32.Parse(parms[1]));
                        }
                    }
                }
                else if (firstWord.Equals("triangle"))
                {
                    String args = lineOfCommand.Substring(8, (lineOfCommand.Length - 8));
                    String[] parms = args.Split(',');
                    for (int i = 0; i < parms.Length; i++)
                    {
                        parms[i] = parms[i].Trim();
                    }
                    DrawTriangle(Int32.Parse(parms[0]), Int32.Parse(parms[1]), Int32.Parse(parms[2]));
                }


            }
            else
            {
                if (firstWord.Equals("loop"))
                {
                    counter = int.Parse(words[1]);
                    int loopStartLine = (GetLoopStartLineNumber());
                    int loopEndLine = (GetLoopEndLineNumber() - 1);
                    loopCounter = loopEndLine;
                    for (int i = 0; i < counter; i++)
                    {
                        for (int j = loopStartLine; j <= loopEndLine; j++)
                        {
                            String oneLineCommand = txtCodeArea.Lines[j];
                            oneLineCommand = oneLineCommand.Trim();
                            if (!oneLineCommand.Equals(""))
                            {
                                RunCommand(oneLineCommand);
                            }
                        }
                    }
                }
                else if (firstWord.Equals("if"))
                {
                    Boolean loop = false;
                     
                    if (words[1].ToLower().Equals("radius"))
                    {
                        if (radius == int.Parse(words[1]))
                        {
                            loop = true;
                        }
                    }
                    else if (words[1].ToLower().Equals("breadth"))
                    {
                        if (breadth == int.Parse(words[1]))
                        {
                            loop = true;
                        }
                    }
                    else if (words[1].ToLower().Equals("length"))
                    {
                        if (length == int.Parse(words[1]))
                        {
                            loop = true;
                        }

                    }
                    else if (words[1].ToLower().Equals("counter"))
                    {
                        if (counter == int.Parse(words[1]))
                        {
                            loop = true;
                        }
                    }
                    else if (words[1].ToLower().Equals("num"))
                    {
                        if (counter == int.Parse(words[1]))
                        {
                            loop = true;
                        }
                    }

                    int ifStartLine = (GetIfStartLineNumber());
                    int ifEndLine = (GetEndifEndLineNumber() - 1);
                    loopCounter = ifEndLine;
                    if (loop)
                    {
                        for (int j = ifStartLine; j <= ifEndLine; j++)
                        {
                            String oneLineCommand = txtCodeArea.Lines[j];
                            oneLineCommand = oneLineCommand.Trim();
                            if (!oneLineCommand.Equals(""))
                            {
                                RunCommand(oneLineCommand);
                            }
                        }
                    }
                }
            }
        }



        /// <summary>
        /// Initiating if the is an if clause
        /// </summary>
        /// <returns></returns>

        private int GetIfStartLineNumber()
        {
            int numberOfLines = txtCodeArea.Lines.Length;
            int lineNum = 0;

            for (int i = 0; i < numberOfLines; i++)
            {
                String oneLineCommand = txtCodeArea.Lines[i];
                oneLineCommand = Regex.Replace(oneLineCommand, @"\s+", " ");
                string[] words = oneLineCommand.Split(' ');
                //removing white spaces in between words
                for (int j = 0; j < words.Length; j++)
                {
                    words[j] = words[j].Trim();
                }
                String firstWord = words[0].ToLower();
                oneLineCommand = oneLineCommand.Trim();
                if (firstWord.Equals("if"))
                {
                    lineNum = i + 1;

                }
            }
            return lineNum;
        }
        /// <summary>
        /// checks the endif
        /// </summary>
        /// <returns></returns>

        private int GetEndifEndLineNumber()
        {
            int numberOfLines = txtCodeArea.Lines.Length;
            int lineNum = 0;

            for (int i = 0; i < numberOfLines; i++)
            {
                String oneLineCommand = txtCodeArea.Lines[i];
                oneLineCommand = oneLineCommand.Trim();
                if (oneLineCommand.ToLower().Equals("endif"))
                {
                    lineNum = i + 1;

                }
            }
            return lineNum;
        }

        /// <summary>
        /// checks endloop 
        /// </summary>
        /// <returns></returns>
        private int GetLoopEndLineNumber()
        {
            try
            {
                int numberOfLines = txtCodeArea.Lines.Length;
                int lineNum = 0;

                for (int i = 0; i < numberOfLines; i++)
                {
                    String oneLineCommand = txtCodeArea.Lines[i];
                    oneLineCommand = oneLineCommand.Trim();
                    if (oneLineCommand.ToLower().Equals("endloop"))
                    {
                        lineNum = i + 1;

                    }
                }
                return lineNum;
            }
            catch
            {
                return 0;
            }
        }
        /// <summary>
        /// checks the loop 
        /// </summary>
        /// <returns></returns>
        private int GetLoopStartLineNumber()
        {
            int numberOfLines = txtCodeArea.Lines.Length;
            int lineNum = 0;

            for (int i = 0; i < numberOfLines; i++)
            {
                String oneLineCommand = txtCodeArea.Lines[i];
                oneLineCommand = Regex.Replace(oneLineCommand, @"\s+", " ");
                string[] words = oneLineCommand.Split(' ');
                //removing white spaces in between words
                for (int j = 0; j < words.Length; j++)
                {
                    words[j] = words[j].Trim();
                }
                String firstWord = words[0].ToLower();
                oneLineCommand = oneLineCommand.Trim();
                if (firstWord.Equals("loop"))
                {
                    lineNum = i + 1;

                }
            }
            return lineNum;

        }

        private void btnrun_Click(object sender, EventArgs e)
        {
            hasDrawOrMoveValue = false;
            //checking if the code box is empty
            if (txtCodeArea.Text != null && txtCodeArea.Text != "")
            {
                MessageBox.Show("Successful.... Click on OK to see the result!!");
                loadCommand();
            }
        }

        private void txtCodeArea_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// method draw triangle
        /// </summary>
        /// <param name="rBase"></param>
        /// <param name="adj"></param>
        /// <param name="hyp"></param>
        private void DrawTriangle(int rBase, int adj, int hyp)
        {
            Pen myPen = new Pen(backcolor);
            Point[] pnt = new Point[3];

            pnt[0].X = moveX;
            pnt[0].Y = moveY;

            pnt[1].X = moveX - rBase;
            pnt[1].Y = moveY;

            pnt[2].X = moveX;
            pnt[2].Y = moveY - adj;
            g.DrawPolygon(myPen, pnt);
        }

        /// <summary>
        /// method to draw rectangle
        /// </summary>
        /// <param name="breadth"></param>
        /// <param name="height"></param>
        private void DrawRectangle(int breadth, int length)
        {
            Pen myPen = new Pen(backcolor);
            g.DrawRectangle(myPen, moveX - breadth / 2, moveY - length / 2, breadth, length);
        }


        /// <summary>
        /// method to draw circle
        /// </summary>
        /// <param name="radius"></param>
        private void DrawCircle(int radius)
        {
            Pen myPen = new Pen(backcolor);
            g.DrawEllipse(myPen, moveX - radius, moveY - radius, radius * 2, radius * 2);
        }
        


        private void txtExecutionCmd_TextChanged(object sender, EventArgs e)
        {
            actionCmd = txtExecutionCmd.Text.ToLower();
           
            switch (actionCmd)
            {
                case "run":
                    try
                    {
                        program = txtCodeArea.Text.ToLower();
                        char[] delimiters = new char[] { '\r', '\n' };
                        string[] parts = program.Split(delimiters, StringSplitOptions.RemoveEmptyEntries); //holds invididuals code line on the basis of delimiters
                        consoletext = "Program code: \n";
                        foreach (string part in parts)
                        {
                            consoletext += part + "\n";
                        }
                        consoletext += "\n\n";


                        //loop through the whole program code line
                        for (int i = 0; i < parts.Length; i++)
                        {
                            //single code line
                            String code_line = parts[i];

                            char[] code_delimiters = new char[] { ' ' };
                            words = code_line.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries); //holds invididuals code line

                            //condition to check if "draw" then
                            if (words[0].Equals("draw"))
                            {
                                if (words[1] == "circle") // condition to check if "circle" then
                                {
                                    if (!(words.Length == 3)) //checks if written code is correct or not
                                    {
                                        MessageBox.Show("Enter correct command and parameter");
                                        consoletext += "View the correct Command: \n e.g. draw circle 100 or draw circle r \n\n";
                                    }
                                    else
                                    {
                                        if (circleObjects.Exists(x => x.getX() == moveX && x.getY() == moveY
                                        && x.getRadius() == Convert.ToInt32(words[2])) == true)
                                        //check for the x,y,radius exists or not
                                        {
                                            consoletext += "!!circle already exists with the given value!!\n\n";

                                        }
                                        else
                                        {
                                            //Create a new circle
                                            Circle circle = new Circle();
                                            circle.setX(moveX);
                                            circle.setY(moveY);
                                            circle.setRadius(Convert.ToInt32(words[2]));
                                            circleObjects.Add(circle);
                                            drawCircle = true;
                                            consoletext += "Adding new circle\n\n";
                                            MessageBox.Show("Click Ok to see the result");
                                        }
                                    }
                                }
                                if (words[1].Equals("rectangle"))
                                {
                                    //MessageBox.Show(moveX.ToString());
                                    if (!(words.Length == 4)) //extending parameter values
                                    {
                                        MessageBox.Show("Enter correct command");
                                        consoletext += "View the correct Command:: \n e.g. draw rectangle 100 100 or draw circle h w \n\n";
                                    }
                                    else
                                    {
                                        if (rectangleObjects.Exists(x => x.getX() == moveX && x.getY() == moveY
                                        && x.getLength() == Convert.ToInt32(words[2]) && x.getBreadth() ==
                                        Convert.ToInt32(words[3])) == true)//check for the x,y,radius exists or not
                                        {
                                            consoletext += "!!rectangle object exists with given parameters!!\n\n";
                                        }
                                        else
                                        {
                                            //create a new rectangle
                                            Rectangle rect = new Rectangle();
                                            rect.setX(moveX);
                                            rect.setY(moveY);
                                            rect.setLength(Convert.ToInt32(words[2]));
                                            rect.setBreadth(Convert.ToInt32(words[3]));
                                            rectangleObjects.Add(rect);
                                            drawRectangle = true;
                                            consoletext += "Adding new rectangle\n\n";
                                            MessageBox.Show("Click Ok to see the result");
                                        }
                                    }
                                }

                                else if (words[1].Equals("polygon"))
                                {
                                    drawPolygon = true;
                                }
                            }
                            if (words[0] == "moveto") // condition to check if "move" then
                            {
                                moveX = Convert.ToInt32(words[1]);
                                moveY = Convert.ToInt32(words[2]);
                                consoletext += "X=" + moveX + "\n" + "Y=" + moveY + "\n\n";
                            }
                            if (words[0] == "color")
                            {
                                thickness = Convert.ToInt32(words[2]);

                                if (words[1] == "red")
                                {
                                    color = Color.Red;
                                    consoletext += "Pen is of red color\n\n";
                                }
                                else if (words[1] == "blue")
                                {
                                    color = Color.Blue;
                                    consoletext += "Pen is of blue color\n\n";
                                }
                                else if (words[1] == "yellow")
                                {
                                    color = Color.Yellow;
                                    consoletext += "Pen is of yellow color\n\n";
                                }
                                else
                                {
                                    color = Color.Green;
                                    consoletext += "Pen is of green color\n\n";
                                }
                            }
                        }
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        consoletext += "Error: " + ex.Message + "\n\n";
                    }
                    catch (FormatException ex)
                    {
                        consoletext += "!!Please input correct parameter!!\n\n";
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        consoletext += "!!Please input correct parameter!!\n\n";
                    }
                    pnlDisplayOutput.Refresh(); //refresh with every drawing equals to true
                    break;

                case "clear":
                    circleObjects.Clear();
                    rectangleObjects.Clear();
                    moveObjects.Clear();
                    polygonObjects.Clear();
                    this.drawPolygon = false;
                    this.drawCircle = false;
                    this.drawRectangle = false;
                    this.txtCodeArea.Clear();
                    pnlDisplayOutput.Refresh();
                    break;


                case "reset":
                    moveX = 0;
                    moveY = 0;
                    moveObjects.Clear();
                    break;



            }
        }
        
        private void pnlDisplayOutput_Paint_1(object sender, PaintEventArgs e)
        {
            //graphics to draw in panel
            Graphics g = e.Graphics;
            if (drawCircle == true)//condition to draw circle
            {
                foreach (Circle circleObject in circleObjects)
                {
                    consoletext += "Drawing Circle\n\n";
                    circleObject.draw(g, color, thickness);//with the given graphics draw circle

                }
            }

            // condition to draw rectangle

            if (drawRectangle == true) 
            {
                foreach (Rectangle rectangleObject in rectangleObjects)
                {
                    consoletext += "Drawing Rectangle\n\n";
                    rectangleObject.draw(g, color,thickness );// with the given graphics draw rectangle
                }
            }
            //condition to draw triangle with polygon points
            if (drawPolygon == true)
            {
                Pen blackPen = new Pen(color);
                PointF point1 = new PointF(50.0F, 50.0F);
                PointF point2 = new PointF(170.0F, 150.0F);
                PointF point3 = new PointF(20.0F, 250.0F);
                string[] str = new string[3];
                PointF[] curvePoints =
                {
                    point1,
                    point2,
                    point3
                };
                e.Graphics.DrawPolygon(blackPen, curvePoints);
            }

        }
        

    }
}