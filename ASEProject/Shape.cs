﻿using System;///you are using the System library in your project
using System.Collections.Generic;///allow users to create strongly typed collections that provide better type safety and performance than non-generic strongly typed collections
using System.Linq;/// Provides classes and interfaces that support queries that use Language-Integrated Query (LINQ)
using System.Text;/// Contains classes that represent ASCII and Unicode character encodings
using System.Threading.Tasks; ///Provides a pool of threads that can be used to execute tasks, post work items, process asynchronous I/O
using System.Drawing;

namespace AseProject //namespaces are used to logically arrange classes, structs, interfaces, enums and delegates
{
   public abstract class Shape //creating abstract class called Shap
    {
        ///Declearing variable x,y,a and initialize to 0
        protected int x = 0, y = 0, z = 0;


        /// <summary>
        /// parameterized constructor
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// </summary>
        public Shape(int x, int y) ///Public class Shape with int x and y parameter
        {
            this.x = x;
            this.y = y;
        }
        public Shape( int x, int y, int z) ///Public class Shape with int x,y and z parameter
        {

            this.x= x;
            this.y=y;
            this.z=z;
        }
        ///<summary>
        /// default constructor 
        ///</summary>
        public Shape()
        {

        }
        ///<summary>
        ///X setter
        ///<param name ="x"></param>
        ///</summary>
        
        public void setX(int x)
        {
            this.x = x;
           
        }
        /// <summary>
        ///Y setter
        /// </summary>
        /// <param name="y"></param>
        public void setY(int y)
        {
            this.y = y;
        }
        /// <summary>
        /// X getter
        /// </summary>
        /// <returns></returns>
        public int getX()
        {
            return x; ///return x
            
        }
        /// <summary>
        /// Y getter
        /// <returns></returns>
        /// </summary>
        public int getY()
        {
            return y; ///return y
        }

        /// <summary>
        /// draw method
        /// <param name="g"></param>
        /// </summary>
        public abstract void draw(Graphics g, Color color, int thickness);//any derived class must implement this method and this the the draw method
    }
}
