﻿using System;///you are using the System library in your project
using System.Collections.Generic;///allow users to create strongly typed collections that provide better type safety and performance than non-generic strongly typed collections
using System.Drawing;///The Graphics class provides methods for drawing to the display device. Classes such as Rectangle and Point encapsulate GDI+ primitives
using System.Linq;/// Provides classes and interfaces that support queries that use Language-Integrated Query (LINQ)
using System.Text;/// Contains classes that represent ASCII and Unicode character encodings
using System.Threading.Tasks; ///Provides a pool of threads that can be used to execute tasks, post work items, process asynchronous I/O

namespace AseProject ///namespaces are used to logically arrange classes, structs, interfaces, enums and delegates
{
    class Polygon : Shape //creating class Polygon
    {
        public Polygon() ///making class public
        {

        }
        public PointF[] polygon_vertices { get; set; }
        public override void draw(Graphics g, Color color, int thickness) ///drawing function for polygon
        { 
            Pen p = new Pen(color, thickness); /// p for the color and thickness of pen
            g.DrawPolygon(p, polygon_vertices);

         
        }
    }
}
