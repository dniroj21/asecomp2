﻿using System; ///you are using the System library in your project
using System.Collections.Generic;///allow users to create strongly typed collections that provide better type safety and performance than non-generic strongly typed collections
using System.Linq; /// Provides classes and interfaces that support queries that use Language-Integrated Query (LINQ)
using System.Text; /// Contains classes that represent ASCII and Unicode character encodings
using System.Threading.Tasks; ///Provides a pool of threads that can be used to execute tasks, post work items, process asynchronous I/O
using System.Drawing; ///The Graphics class provides methods for drawing to the display device. Classes such as Rectangle and Point encapsulate GDI+ primitives

namespace AseProject ///namespaces are used to logically arrange classes, structs, interfaces, enums and delegates
{
    class Rectangle : Shape  //class Rectangle define
    {
        ///variable declarations
        int length, breadth; ///integer veriable for length and breadth
        
        /// Parameterized constructor        
        public Rectangle(int x, int y, int length, int breadth) : base(x, y)
        {
            this.length = length;
            this.breadth = breadth;
        }
        /// another parameterized constructor        
        public Rectangle(int x, int y) : base(x, y)
        {

        }
        ///default constructor
        public Rectangle()
        {

        }

        ///<summary>
        /// setter
        ///</summary>
        ///<param name="length"></param>
        public void setLength(int length)
        {
            this.length = length; ///this constructer
        }
        ///<summary>
        ///getter
        ///</summary>
        ///<returns></returns>
        public int getLength()
        {
            return this.length;

        }
        ///<summary>
        /// setter
        ///</summary>
        ///<param name="breadth"></param>
        public void setBreadth(int breadth)
        {
            this.breadth = breadth;
        }
        ///<summary>
        /// getter
        ///</summary>
        ///<returns></returns>
        public int getBreadth()
        {
            return this.breadth;

        }

        /// <summary>
        /// draw method
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g, Color color, int thickness) 
        {
            Pen p = new Pen(color, thickness);
            g.DrawRectangle(p, x, y, length, breadth); ///draw a rectengale by using specific coordinator
        }
    }
}