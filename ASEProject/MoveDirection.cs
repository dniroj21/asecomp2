﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AseProject
{
    /// <summary>
    /// MoveDirection class
    /// </summary>
    class MoveDirection
    {
       ///<summary>
       ///move default constructor
       ///</summary>
      public MoveDirection()
      {

      }
        ///<summary>
        ///setter and getter
        ///</summary>
       public int x { get; set; }
       public int y { get; set; }
    }
}
