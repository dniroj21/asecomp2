﻿using System;///you are using the System library in your project
using System.Collections.Generic;///allow users to create strongly typed collections that provide better type safety and performance than non-generic strongly typed collections
using System.Linq;/// Provides classes and interfaces that support queries that use Language-Integrated Query (LINQ)
using System.Text;/// Contains classes that represent ASCII and Unicode character encodings
using System.Threading.Tasks; ///Provides a pool of threads that can be used to execute tasks, post work items, process asynchronous I/O

namespace AseProject //namespaces are used to logically arrange classes, structs, interfaces, enums and delegates
{
    public class ShapeFactory : AbstractFactory ///public class define
    {
        /// <summary>
        /// Method to get the required shape
        /// </summary>
        /// <param name="shapeType"></param>
        /// <returns></returns>
        public override Shape getShape(string shapeType)
        {
            ///If else condition for  getting shape
            if(shapeType== null) 
            {
                return null; ///if condition  shapeType equal to null return null
            }
            if (shapeType.Equals("Circle"))
            {
                return new Circle(50, 50, 100); ///if condition  shapeType equal to Eualals('Circle') return Circle

            }
            else if (shapeType.Equals("Rectangle"))
            {
                return new Rectangle(50, 50, 50, 50); ///if condition  shapeType equal to Eualals('Rectangle') return Rectangle

            }

            return null; //it will retuen null value
        }

           
    }
}
