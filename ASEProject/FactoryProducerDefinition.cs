﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AseProject
{
    /// <summary>
    /// Simple Factory Definition class
    /// </summary>
   public  class FactoryProducerDefinition
   {
        ///<summary>
        ///checks wether the type is Shape or Color
        ///</summary>
        ///<param name="type"></param>
        ///<returns></returns>
        public bool isShape(string type)
        {
            if(type == "shape")
            {
                return true;
            }
            return false;
        }
    }
}
