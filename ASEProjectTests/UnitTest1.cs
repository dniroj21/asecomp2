using AseProject;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AseProjectTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void IsShapeEquals_Rectangle_ReturnTrue()
        {
            var shapeFactoryDef = new ShapeFactoryDefinition();
            bool result = shapeFactoryDef.isRectangle("rectangle");
            
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void IsShapeEquals_Circle_ReturnTrue()
        {
            var ShapeFactoryDef = new ShapeFactoryDefinition();
            bool result = ShapeFactoryDef.isCircle("circle");
            
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void ToCheckWhetherTheTypeIS_Shape_ReturnTrue()
        {
            var factoryProducerDef = new FactoryProducerDefinition();
            bool result = factoryProducerDef.isShape("shape");

            Assert.AreEqual(true, result);
        }


    }
}

